{ stdenv, lib, runCommand, fetchurl, fetchFromGitHub, haskell, python3, zlib }:

let
  src = fetchFromGitHub {
    owner = "haskell";
    repo = "Cabal";
    rev = "cabal-install-3.4.0.0-rc3";
    sha256 = "1m1355863s4cj9v4r28b6d4kkhh3m7h6gjn1ziffl76r0xpk94rh";
  };

  plan = lib.importJSON "${src}/bootstrap/linux-8.10.1.json";

  sources = lib.listToAttrs (lib.concatMap (dep:
      [
        { name = "${dep.package}-${dep.version}.tar.gz";
          value = fetchurl {
            url = "https://hackage.haskell.org/package/${dep.package}/${dep.package}-${dep.version}.tar.gz";
            sha256 = dep.src_sha256;
          };
        }
      ] ++ lib.optional (dep.revision != null) {
        name = "${dep.package}.cabal";
        value = fetchurl {
          url = "https://hackage.haskell.org/package/${dep.package}-${dep.version}/revision/${toString dep.revision}.cabal";
          sha256 = dep.cabal_sha256;
        };
      }
    )
    (lib.filter (dep: dep.src_sha256 != null) plan.dependencies));

  sourcesDir = runCommand "sources" {} (
    ''
      mkdir -p $out;
    '' +
    lib.concatStringsSep "\n" (lib.mapAttrsToList (name: src: "cp ${src} $out/${name};") sources)
  );

  cabal-install = stdenv.mkDerivation {
    name = "cabal-install";
    inherit src;
    nativeBuildInputs = [ haskell.compiler.ghc8101 python3 ];
    buildInputs = [ zlib ];
    patchPhase = ''
      mkdir -p _build
      ln -s ${sourcesDir} _build/tarballs
      ls -ls _build _build/tarballs _build/tarballs/*
    '';
    buildPhase = ''
      python3 bootstrap/bootstrap.py -d bootstrap/linux-8.10.1.json
    '';
    installPhase = ''
      mkdir -p $out/bin
      mv _build/bin/cabal $out/bin/cabal
    '';
  };

in cabal-install
