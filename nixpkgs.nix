{ fetchFromGitHub }:

fetchFromGitHub {
  owner = "nixos";
  repo = "nixpkgs";
  rev = "ffb3aab257e8851b558cdc6079241a7eb0c7239e";
  sha256 = "sha256:147d4nsq0334skyr9rghcfnizasvba1pr2rq72axwdldlma0qli1";
}
